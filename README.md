# wHTML
A demonstration of how HTML can be extended to support custom tag definitions or widgets. A widget encapsulates the markup, default styling, and behavior of a reusable UI component.

## Materials
* Proposal (https://docs.google.com/document/d/1lj8vb-cDbDlNVeCks5RfSpQ3NFuME55U3b_PanP6wnQ/edit)
* Design Document (https://docs.google.com/document/d/1QB5sh7sLonSxeeVXUZq-r7U2NmnrapvyN26mXry9t4s/edit)
* Slides from Talk (https://docs.google.com/presentation/d/13bKB52cPhwgPfxTrOSeZ-kFKHkZtZ_rYNVD0xusN234/edit)
* Slides from Demo (https://docs.google.com/presentation/d/1TUKUzsyv7_D7_nYGPbxlwo257NupifEb1iDRzyfvVGQ/edit)
* Screencast (http://youtu.be/sQvQWb-KUsA)

## License
Libraries pulled in under the vendor directories are licensed under their own respective terms.

A significant portion of the CSS is derived from Twitter Bootstrap:

    Copyright 2012 Twitter, Inc.

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

All other code is licensed under the MIT License as follows:

    Copyright (c) 2012 Vinson Chuong and David Thorman

    Permission is hereby granted, free of charge, to any person obtaining a copy of
    this software and associated documentation files (the "Software"), to deal in
    the Software without restriction, including without limitation the rights to
    use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is furnished
    to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
    FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
    COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
    IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
    CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
