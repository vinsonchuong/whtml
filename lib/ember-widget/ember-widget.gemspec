$:.push File.expand_path('../lib', __FILE__)

require 'ember-widget/version'

Gem::Specification.new do |s|
  s.name        = 'ember-widget'
  s.version     = Ember::Widget::VERSION
  s.authors     = ['TODO']
  s.email       = ['TODO']
  s.homepage    = 'TODO'
  s.summary     = 'TODO'
  s.description = 'TODO'

  s.files = Dir["{app,config,db,lib}/**/*"]

  s.add_dependency 'rails', '~> 3.2.3'
  s.add_dependency 'sass-rails'
  s.add_dependency 'sprockets'
  s.add_dependency 'ember-rails'
  s.add_dependency 'polyglot'
  s.add_dependency 'treetop'

  s.require_paths = ['lib']
end
