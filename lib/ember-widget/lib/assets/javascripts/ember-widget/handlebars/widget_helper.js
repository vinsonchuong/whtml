Ember.Handlebars.registerHelper('widget', function(path, options) {
  ember_assert("The widget helper only takes a single argument", arguments.length <= 2);

  var
    fn = options.fn,
    data = options.data,
    hash = options.hash,
    id = hash.id;

  if (fn) {
    data.widgetParams = {};
    fn(this);
    Ember.$.extend(options.hash, data.widgetParams);
    delete data.widgetParams;
    delete options.fn;
  }

  if (id) {
    hash.wid = id;
    delete hash.id;
  }

  return Ember.Handlebars.ViewHelper.helper(this, path, options);
});
