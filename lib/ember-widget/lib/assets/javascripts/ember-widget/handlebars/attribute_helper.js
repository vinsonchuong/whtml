Ember.Handlebars.registerHelper('attribute', function(name, options) {
  ember_assert("The attribute helper only takes a single argument", arguments.length <= 2);

  if (options === undefined) {
    options = name;
  }

  var
    hash = Ember.$.extend({}, options.hash),
    type = hash._type,
    data = options.data,
    fn = options.fn,
    parentWidgetParams = data.widgetParams;
  delete hash._type;

  if (type == 'template') {
    parentWidgetParams[name] = Ember.View.create({template: fn, templateData: data, _templateContext: this});
  } else if (type == 'array') {
    if (data.widgetParams instanceof Array) {
      data.widgetParams = hash.content = [];
      parentWidgetParams.push(hash);
      fn(this, options);
    } else {
      parentWidgetParams[name] = data.widgetParams = [];
      fn(this, options);
    }
  } else if (type == 'arrayElement') {
    data.widgetParams = hash;
    parentWidgetParams.push(hash);
    if (fn) fn(this, options);
  }
  data.widgetParams = parentWidgetParams;
});
