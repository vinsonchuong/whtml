#= require ../widget

Ember.Widget.ProgressBar = Ember.Widget.extend
  templateName: 'ember-widget/widget/progress_bar/template'
  classNames: ['ember-widget-progressBar']
  classNameBindings: ['active', 'style']

  active: true
  progress: 50
  widthStyle: Ember.computed(-> 'width: ' + @get('progress') + '%;').property('progress')
  style: ''
