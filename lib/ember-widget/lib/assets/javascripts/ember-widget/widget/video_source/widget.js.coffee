#= require ../widget
get = Ember.get
set = Ember.set

Ember.Widget.VideoSource = Ember.Widget.extend
  tagName: 'source'
  templateName: 'ember-widget/widget/video_source/template'
  attributeBindings: ['src', 'type']
