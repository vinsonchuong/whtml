Ember.Widget = Ember.View.extend Ember.Evented,
  classNames: ['ember-widget']

  parentWidget: Ember.computed(->
    bindingContext = @get('bindingContext')
    if Ember.Widget.detectInstance(bindingContext) then bindingContext else @get('parentView')
  ).property('bindingContext', 'parentView')
  childWidgets: Ember.computed(-> @get('childViews')).property('childViews')

  init: ->
    @_super.apply(@, arguments)
    for  attribute of @
      if attribute.length > 2 and attribute.slice(0, 2) is 'on' and @get(attribute)
        @on(attribute.slice(2), @get('parentWidget'), @get(attribute))

  getChildWidgetById: (id) ->
    childWidgets = this.get('childWidgets')
    for childWidget in childWidgets
      return childWidget if childWidget.get('wid') is id

  setIfUnset: (key, value) -> @set(key, value) unless  @get(key)?

  push: (key, object) -> @getPath(key).pushObject(object)
  pop: (key) -> @getPath(key).popObject()
  each: (key, func) -> @get(key).forEach(func)
  map: (key, property) ->
    if typeof property is 'function'
      @get(key).map(property)
    else
      @get(key).map (item) ->
        if 'get' of item then item.get(property) else item[property]
  filter: (key, property, value) ->
    if typeof property is 'function'
      @getPath(key).filter(property)
    else
      @getPath(key).filterProperty(property, value)
  every: (key, property, value) ->
    if not property?
      property = (item) -> !!item
    @filter(key, property, value).length is @get(key).length
  some: (key, property, value) ->
    if not property?
      property = (item) -> !!item
    !!@filter(key, property, value).length
  remove: (key, object) ->
    if not key?
      @_super()
    else if object instanceof Array
      @getPath(key).removeObjects(object)
    else
      @getPath(key).removeObject(object)
  removeIf: (key, property, value) -> @remove(key, @filter(key, property, value))
  removeAt: (key, index) ->
    attribute = @getPath(key)
    if index instanceof Array
      index.forEach((index) -> attribute.removeAt(index))
    else
      attribute.removeAt(index)
