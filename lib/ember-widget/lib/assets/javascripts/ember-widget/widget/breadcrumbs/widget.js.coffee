#= require ../widget
set = Ember.set
setPath = Ember.setPath

Ember.Widget.Breadcrumbs = Ember.Widget.extend
  tagName: 'ul'
  templateName: 'ember-widget/widget/breadcrumbs/template'
  classNames: ['ember-widget-breadcrumbs']

  items: null

  init: ->
    @_super()
    @setIfUnset('items', [])
    @itemsDidChange()
  itemsDidChange: Ember.observer(->
    @each('items', (item) -> set(item, 'active', false))
    @setPath('items.lastObject.active', true)
  , 'items', 'items.@each')

  pressItem: (event) ->
    @fire('select', {context: event.widget.get('bindingContext')})
