#= require ../widget

Ember.Widget.FormElement = Ember.Widget.extend
  tagName: null,
  classNames: ['ember-widget-formElement']
  attributeBindings: ['disabled']

  disabled: false
  value: ''

  valueDidChange: Ember.observer(->
    @fire('change', {})
  , 'value')
