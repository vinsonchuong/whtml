#= require ../widget

Ember.Widget.FormElement.TextField = Ember.Widget.FormElement.extend
  tagName: 'input'
  classNames: ['ember-widget-formElement-textField']
  attributeBindings: ['maxlength', 'placeholder', 'size', 'type', 'value']

  maxlength: null
  placeholder: null
  size: null
  type: 'text'

  keyUp: (event) ->
    return if @state isnt 'inDOM'
    @fire('keyup', keyCode: event.keyCode)
    @set('value', @$().val())
