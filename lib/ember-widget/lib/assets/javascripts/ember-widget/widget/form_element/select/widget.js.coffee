#= require ../widget
set = Ember.set

Ember.Widget.FormElement.Select = Ember.Widget.FormElement.extend
  tagName: 'select'
  templateName: 'ember-widget/widget/form_element/select/template'
  classNames: ['ember-widget-formElement-select']
  attributeBindings: ['multiple']

  multiple: false
  options: null
  value: Ember.computed((key, value) ->
    if arguments.length is 1
      value = @filter('options', 'selected', true).mapProperty('value')
      value = [null] if not value.length
      if @get('multiple') then value else value[0]
    else
      value = [value] unless @get('multiple')
      @_updateSelected(@map('options', (option) ->
        value.indexOf(option.value) > -1))
  ).property('options')

  init: ->
    @_super();
    @setIfUnset('options', [])
    window.select = this

  change: (event) ->
    return if @state isnt 'inDOM' or event.type isnt 'change'
    @_updateSelected(@$('option').map(->
      $(@).prop('selected')))

  _updateSelected: (selections) ->
    newOptions = []
    @each('options', (option, i) ->
      set(option, 'selected', selections[i])
    )
