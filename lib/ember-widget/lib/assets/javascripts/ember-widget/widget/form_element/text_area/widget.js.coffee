#= require ../widget

Ember.Widget.FormElement.TextArea = Ember.Widget.FormElement.extend
  tagName: 'textarea'
  classNames: ['ember-widget-formElement-textArea']
  attributeBindings: ['cols', 'maxlength', 'placeholder', 'rows']

  cols: null
  maxlength: null
  placeholder: null
  rows: null

  didInsertElement: -> @_updateContent()
  valueDidChange: Ember.observer(->
    @_super.apply(this, arguments)
    @_updateContent()
  , 'value')

  keyUp: -> @set('value', @$().val())

  _updateContent: ->
    return if @state isnt 'inDOM'
    @$().val(@get('value'))
