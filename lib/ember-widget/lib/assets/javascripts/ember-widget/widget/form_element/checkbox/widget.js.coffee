#= require ../widget

Ember.Widget.FormElement.Checkbox = Ember.Widget.FormElement.extend
  tagName: 'input'
  attributeBindings: ['type', 'value:checked']
  classNames: ['ember-widget-formElement-checkbox']

  type: 'checkbox'
  value: false

  change: -> @set('value', @$().prop('checked')) if @state is 'inDOM'
