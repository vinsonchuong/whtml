#= require ../widget

Ember.Widget.Indicator = Ember.Widget.extend
  tagName: 'span'
  templateName: 'ember-widget/widget/indicator/template'
  classNames: ['ember-widget-indicator']
  classNameBindings: ['style']

  content: null
  style: ''
