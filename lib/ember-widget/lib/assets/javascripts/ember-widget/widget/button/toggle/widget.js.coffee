#= require ../widget

Ember.Widget.Button.Toggle = Ember.Widget.Button.extend
  classNames: ['ember-widget-button-toggle']
  classNameBindings: ['value:active']

  value: false

  press: -> @toggleProperty('value')
