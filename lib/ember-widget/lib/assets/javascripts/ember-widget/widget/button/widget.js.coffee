#= require ../widget

Ember.Widget.Button = Ember.Widget.extend
  tagName: 'button'
  templateName: 'ember-widget/widget/button/template'
  classNames: ['ember-widget-button']
  classNameBindings: ['disabled', 'style', 'active']
  attributeBindings: ['disabled', 'href']

  content: ''
  active: false
  disabled: false
  style: null
  href: Ember.computed(->
    if @get('tagName') is 'a' and not @get('disabled') then 'javascript:;' else null
  ).property('tagName', 'disabled')
  contentIsString: Ember.computed(->
    typeof @get('content') is 'string'
  ).property('content')

  click: -> @fire('press', widget: @) unless @get('disabled')
  keyUp: (event) -> @fire('press', widget: @) if event.keycode in [13, 32] and not @get('disabled')
