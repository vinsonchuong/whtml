#= require ../widget
get = Ember.get
set = Ember.set

Ember.Widget.Accordion = Ember.Widget.extend
  tagName: 'ul'
  templateName: 'ember-widget/widget/accordion/template'
  classNames: ['ember-widget-accordion']

  items: null

  init: ->
    @_super();
    @setIfUnset('items', [])

  toggleItem: (event) ->
    selectedItem = event.widget.get('bindingContext')
    itemNodes = @$('> li > .content > div')
    @each('items', (item, i) ->
      isOpen = get(item, 'open')
      Ember.$(itemNodes[i]).each(-> $(@).parent().css('height', if item isnt selectedItem or isOpen then 0 else @offsetHeight))
      set(item, 'open', item is selectedItem and !isOpen))
