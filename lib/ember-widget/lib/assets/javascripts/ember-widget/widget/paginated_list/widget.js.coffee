#= require ../widget

Ember.Widget.PaginatedList = Ember.Widget.extend
  templateName: 'ember-widget/widget/paginated_list/template'
  classNames: ['ember-widget-paginatedList']

  items: null
  page: null
  pageNum: 1
  pageSize: 3
  pageNumbers: Ember.computed(->
    pageNum = @get('pageNum')
    (pageNum: num, open: num == pageNum for num in [1..Math.ceil(@get('items').length / @get('pageSize'))])
  ).property('items', 'items.@each', 'pageNum')
  firstPage: Ember.computed(-> @get('pageNum') is 1).property('pageNum')
  lastPage: Ember.computed(-> @get('pageNum') * @get('pageSize') >= @get('items').length).property('pageNum')

  init: ->
    @_super()
    @setIfUnset('items', [])
    @pageDidChange()

  pageDidChange: Ember.observer(->
    items = @get('items')
    items.setEach('open', false)
    pageSize = @get('pageSize')
    start = (@get('pageNum') - 1) * pageSize
    @get('items')[start...(start + pageSize)].setEach('open', true)
  , 'items', 'items.@each', 'pageNum', 'pageSize')

  prevPage: ->
    @decrementProperty('pageNum')
  nextPage: ->
    @incrementProperty('pageNum')
  changePage: (event) ->
    @set('pageNum', +event.widget.get('bindingContext'))
