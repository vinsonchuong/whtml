#= require ../widget

Ember.Widget.GridColumn = Ember.Widget.extend
  templateName: 'ember-widget/widget/grid_column/template'
  classNames: ['ember-widget-gridColumn']
  classNameBindings: ['spanClass', 'offsetClass']

  span: 12
  offset: 0
  spanClass: 'span12' # Figure out why can't bind class to computed property
  offsetClass: 'offset0'

  didInsertElement: ->
    @_super()
    @setIfUnset('span', 12)
    @setIfUnset('offset', 0)
    @_updateSpan()
    @_updateOffset()
  spanDidChange: Ember.observer(-> @_updateSpan()).property('span')
  offsetDidChange: Ember.observer(-> @_updateOffset()).property('offset')

  _updateSpan: -> @set('spanClass', 'span' + @get('span'))
  _updateOffset: -> @set('offsetClass', 'offset' + @get('offset'))

