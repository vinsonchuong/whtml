#= require ../widget

Ember.Widget.Grid = Ember.Widget.extend
  templateName: 'ember-widget/widget/grid/template'
  classNames: ['ember-widget-grid']
  classNameBindings: ['showGuides:guided']
  attributeBindings: ['style']

  rows: null
  showGuides: false
  width: '940px'
  style: Ember.computed(-> 'width:' + @get('width') + ';').property('width')

  init: ->
    @_super()
    @setIfUnset('rows', [])
