#= require ../widget
get = Ember.get
set = Ember.set

Ember.Widget.Video = Ember.Widget.extend
  templateName: 'ember-widget/widget/video/template'
  sources: null
  lines: null

  currentTime: "0"
  currentLine:( ->
    newLine = ""
    time = @get('currentTime')
    @each('lines', (line) ->
      if line.time < time
        newLine = line.content
    )
    @$('.subtitles').html('<p>' + newLine + '</p>')
  ).observes('currentTime')
    

  init:->
    @_super()
    @setIfUnset('sources', [])
    @setIfUnset('lines', [])
    th = @
    @updateTime = (event) ->  th._updateTime(event)

  didInsertElement: ->
    @_super()
    @_addHandlers()
  willDestroyElement: ->
    @_super()
    @_removeHandlers()

  _addHandlers: ->
    @$('video').on('timeupdate', @updateTime)

  _removeHandlers: ->
    @$('video').off('timeupdate', @updateTime)

  _updateTime: ->
    @set('currentTime', @$('video').prop('currentTime'))

 # negative playback speed doesn't appear to be supported.
 # doubleBackSpeedVideo: ->
 #   @$('video').prop('playbackRate', -2.0)
  halfSpeedVideo: ->
    @$('video').prop('playbackRate', 0.5)
  normalSpeedVideo: ->
    @$('video').prop('playbackRate', 1.0)
  doubleSpeedVideo: ->
    @$('video').prop('playbackRate', 2.0)
  
  
