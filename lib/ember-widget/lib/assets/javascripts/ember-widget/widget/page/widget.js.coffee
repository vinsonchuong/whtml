#= require ../widget

Ember.Widget.Page = Ember.Widget.extend
  templateName: 'ember-widget/widget/page/template'
  classNames: ['ember-widget-page']

  content: null

  didInsertElement: ->
    Ember.$('html').addClass('ember-widget-page')
  willDestroyElement: ->
    Ember.$('html').removeClass('ember-widget-page')
