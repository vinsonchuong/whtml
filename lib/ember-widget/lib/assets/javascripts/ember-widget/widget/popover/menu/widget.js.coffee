#= require ../widget

Ember.Widget.Popover.Menu = Ember.Widget.Popover.extend
  templateName: 'ember-widget/widget/popover/menu/template'
  classNames: ['ember-widget-popover-menu']

  groups: null
  position: 'bottom'
  trigger: 'manual'

  init: ->
    @_super()
    @setIfUnset('items', [])
  didInsertElement: ->
    @_super()
    @$('> .content').on('click', @openPopover)
  willDestroyElement: ->
    @_super()
    @$('> .content').on('click', @openPopover)

  selectItem: (event) ->
    @fire('select', {context: event.widget.get('bindingContext')})

  _openPopover: (event) ->
    @_super()
    event.stopPropagation()
    $('html').one('click', @closePopover)
