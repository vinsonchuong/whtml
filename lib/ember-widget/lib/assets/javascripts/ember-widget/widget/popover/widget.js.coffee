#= require ../widget

Ember.Widget.Popover = Ember.Widget.extend
  templateName: 'ember-widget/widget/popover/template'
  classNames: ['ember-widget-popover']
  classNameBindings: ['position', 'open']

  wrap: null
  content: null
  open: false
  position:'right'
  trigger: 'hover'

  init: ->
    @_super()
    th = @
    @openPopover = (event) -> th._openPopover(event)
    @closePopover = (event) -> th._closePopover(event)
  didInsertElement: ->
    @_super()
    @_addHandlers() if @get('trigger') isnt 'manual'
  triggerDidChange: Ember.observer(->
    @_removeHandlers()
    @_addHandlers() if @get('trigger') isnt 'manual'
  , 'openOnHover')
  willDestroyElement: ->
    @_super()
    @_removeHandlers()

  _addHandlers: ->
    trigger = @get('trigger')
    if trigger is 'hover'
      @_getTriggerNode()
        .on('mouseenter', @openPopover)
        .on('mouseleave', @closePopover)
    else if trigger is 'focus'
      @_getTriggerNode()
        .on('focus', @openPopover)
        .on('blur', @closePopover)
  _removeHandlers: ->
    @$('> .content')
      .off('mouseenter', @openPopover)
      .off('focus', @openPopover)
      .off('mouseleave', @closePopover)
      .off('blur', @openPopover)
    .children().first()
      .off('mouseenter', @openPopover)
      .off('focus', @openPopover)
      .off('mouseleave', @closePopover)
      .off('blur', @openPopover)
  _getTriggerNode: ->
    content = @$('> .content')
    children = content.children().children()
    if children.length == 1 then children else content

  _openPopover: ->
    content = @$('> .content')
    cWidth = content[0].offsetWidth
    cHeight = content[0].offsetHeight
    popover = @$('> .popover')
    pWidth = popover[0].offsetWidth
    pHeight = popover[0].offsetHeight
    position = @get('position')
    if position is 'top'
      popover.css(top: -pHeight, left: cWidth / 2 - pWidth / 2)
    else if position is 'right'
      popover.css(top: cHeight / 2 - pHeight / 2, left: cWidth)
    else if position is 'bottom'
      popover.css(top: cHeight, left: cWidth / 2 - pWidth / 2)
    else if position is 'left'
      popover.css(top: cHeight / 2 - pHeight / 2, left: -pWidth)
    @set('open', true)
  _closePopover: -> @set('open', false)
