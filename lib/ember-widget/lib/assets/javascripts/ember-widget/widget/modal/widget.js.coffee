#= require ../widget

Ember.Widget.Modal = Ember.Widget.extend
  templateName: 'ember-widget/widget/modal/template'
  classNames: ['ember-widget-modal']
  classNameBindings: ['open']

  header: null
  body: null
  footer: null
  open: false

  close: -> @set('open', false)
