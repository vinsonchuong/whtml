#= require ../widget
get = Ember.get
set = Ember.set
setProperties = Ember.setProperties

Ember.Widget.Carousel = Ember.Widget.extend
  templateName: 'ember-widget/widget/carousel/template'
  classNames: ['ember-widget-carousel']

  items: null # Should have atleast 4 elements.

  init: ->
    @_super()
    @setIfUnset('items', [])
  didInsertElement: ->
    @_super()
    items = @get('items')
    return unless items.length
    set(items[0], 'status', 'open')
    return unless items.length > 1
    set(items[items.length - 1], 'status', 'prev')
    set(items[1], 'status', 'next')

  left: ->
    items = @get('items')
    return unless items.length > 1
    numItems = items.length
    openIndex = null
    @each('items', (item, i) -> openIndex = i if get(item, 'status') is 'open')
    set(items[@_mod(openIndex + 1, numItems)], 'status', null)
    set(items[@_mod(openIndex - 2, numItems)], 'status', 'prev')
    set(items[@_mod(openIndex - 1, numItems)], 'status', 'open')
    set(items[openIndex], 'status', 'next')
  right: ->
    items = @get('items')
    return unless items.length > 1
    numItems = items.length
    openIndex = null
    @each('items', (item, i) -> openIndex = i if get(item, 'status') is 'open')
    set(items[@_mod(openIndex - 1, numItems)], 'status', null)
    set(items[openIndex], 'status', 'prev')
    set(items[@_mod(openIndex + 1, numItems)], 'status', 'open')
    set(items[@_mod(openIndex + 2, numItems)], 'status', 'next')

  _mod: (n, base) ->
    n = n + base while n < 0
    n % base
