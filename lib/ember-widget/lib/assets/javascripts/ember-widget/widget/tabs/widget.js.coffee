#= require ../widget
get = Ember.get
set = Ember.set

Ember.Widget.Tabs = Ember.Widget.extend
  templateName: 'ember-widget/widget/tabs/template'
  classNames: ['ember-widget-tabs']
  classNameBindings: ['position']

  position: 'top'
  items: null
  selectedItem: null
  selectedIndex: Ember.computed((key, value) ->
    if arguments.length is 1
      @get('items').indexOf(@get('selectedItem'))
    else
      @_selectTab(@get('items')[value])
      value
  ).property('selectedItem')
  tabsFirst: Ember.computed(-> @get('position') isnt 'bottom').property('position')

  init: ->
    @_super()
    @setIfUnset('items', [])

  selectTab: (event) ->
    @_selectTab(event.widget.get('bindingContext'))

  _selectTab: (item) ->
    @each('items', (item) -> set(item, 'open', false))
    if @get('selectedItem') is item
      @set('selectedItem', null)
    else
      set(item, 'open' , true)
      @set('selectedItem', item)
