#= require ../widget
get = Ember.get
set = Ember.set

Ember.Widget.ButtonGroup = Ember.Widget.extend
  tagName: 'ul'
  templateName: 'ember-widget/widget/button_group/template'
  classNames: ['ember-widget-buttonGroup']

  buttons: null
  type: null

  init: ->
    @_super()
    @setIfUnset('buttons', [])

  press: (event) ->
    context = event.widget.get('bindingContext')
    type = @get('type')
    if type is 'checkbox'
      set(context, 'active', !get(context, 'active'))
    else if type is 'radio'
      @each('buttons', (button) -> set(button, 'active', false))
      set(context, 'active', true)
    handler = context.onpress
    handler() if typeof handler is 'function'

