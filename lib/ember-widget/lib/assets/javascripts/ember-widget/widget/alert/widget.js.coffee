#= require ../widget

Ember.Widget.Alert = Ember.Widget.extend
  templateName: 'ember-widget/widget/alert/template'
  classNames: ['ember-widget-alert']
  classNameBindings: ['open']

  content: null
  contentIsString: Ember.computed(-> typeof @get('content') is 'string').property('content')
  open: false

  didInsertElement: ->
    @_super()
    @openHasChanged() if @get('open')
  openHasChanged: Ember.observer(->
    @$().css('height', if @get('open') then @$('> .content')[0].offsetHeight else '')
  , 'open')

  close: -> @set('open', false)
