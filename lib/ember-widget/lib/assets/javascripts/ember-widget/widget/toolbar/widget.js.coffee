#= require ../widget

Ember.Widget.Toolbar = Ember.Widget.extend
  tagName: 'ul'
  templateName: 'ember-widget/widget/toolbar/template'
  classNames: ['ember-widget-toolbar']

  groups: null

  init: ->
    @_super()
    @setIfUnset('groups', [])
