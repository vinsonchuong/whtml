require 'tilt/template'
require 'ember-widget/desugarer'

module Ember
  module Widget
    class Template < Tilt::Template
      def initialize_engine
      end

      def prepare
      end

      def evaluate(scope, locals, &block)
        Desugarer::parse(data)
      end
    end
  end
end