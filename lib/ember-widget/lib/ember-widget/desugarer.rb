require 'polyglot'
require 'treetop'
require 'ember-widget/grammar'

module Ember
  module Widget
    module Desugarer
      def self.parse(input)
        result = GrammarParser.new.parse input
        result && result.content
      end
    end
  end
end