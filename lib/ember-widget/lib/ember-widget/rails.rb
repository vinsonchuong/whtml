require 'sprockets'
require 'sprockets/engines'
require 'ember-widget/view_helpers'

module Ember
  module Widget
    class Engine < ::Rails::Engine
      initializer :setup_desugarer, :group => :all do |app|
        app.assets.register_engine '.sugar', Template
      end
    end

    class Railtie < ::Rails::Railtie
      initializer 'ember-widget.view_helpers' do
        ActionView::Base.send :include, ViewHelpers
      end
    end
  end
end