require 'ember-widget/desugarer'

module Ember
  module Widget
    module ViewHelpers
      def handlebars_template(&block)
        content = Desugarer::parse(with_output_buffer(&block)).html_safe
        content_tag(:script, content, :type => 'text/x-handlebars')
      end
    end
  end
end