App.Widget.Task = Ember.Widget.extend
  tagName: 'li'
  templateName: 'ember/widgets/task/template'
  classNames: ['app-widget-task']
  classNameBindings: ['finished']

  finished: false
  name: ''

  pressRemove: -> @fire('pressremove', taskWidget: @)
