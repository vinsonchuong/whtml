App.Widget.TaskList = Ember.Widget.extend
  templateName: 'ember/widgets/task_list/template'
  classNames: ['app-widget-taskList']

  tasks: null
  unfinishedCount: Ember.computed(->
    @filter('tasks', 'finished', false).length
  ).property('tasks', 'tasks.@each.finished')

  init: ->
    @_super()
    @setIfUnset('tasks', [])

  addTask: (event) ->
    if event.keyCode is 13
      input = @getChildWidgetById('input')
      @push('tasks', name: input.get('value'), finished: false)
      input.set('value', '')
  removeTask: (event) -> @remove('tasks', event.taskWidget.get('bindingContext'))
  removeFinished: -> @removeIf('tasks', 'finished', true)
