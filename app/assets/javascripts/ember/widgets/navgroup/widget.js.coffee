App.Widget.NavGroup = Ember.Widget.extend
  templateName: 'ember/widgets/navgroup/template'
  tagName: 'ul'
  classNameBindings: ['groupClass']
  
  navItems:null
  position: null
  groupClass: Ember.computed(->
    baseClass = "nav"
    _position = @get('position')
    if _position?
      if _position is "left"
        return baseClass+" pull-left"
      else if _position is "right"
        return baseClass+" pull-right"
    else
      return baseClass)
  init:->
    @_super();
    @setIfUnset('navItems', []);
