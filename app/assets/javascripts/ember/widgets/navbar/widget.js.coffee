App.Widget.NavBar = Ember.Widget.extend
  templateName: 'ember/widgets/navbar/template'
  classNameBindings: ['navClass']

  position: null
  navGroups: null
  navClass: Ember.computed(->
    baseClass = 'navbar'
    position = @get('position')
    if position?
      if position is 'top'
        return baseClass + ' navbar-fixed-top'
      else if position is 'bottom'
        return baseClass + ' navbar-fixed-bottom'
      else
        return baseClass
    else
      return baseClass)
  brandLink: ''
  brandName: ''
  
  init: ->
    @_super()
    @setIfUnset('navGroups', [])
