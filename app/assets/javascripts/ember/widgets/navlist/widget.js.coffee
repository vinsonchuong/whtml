App.Widget.NavList = Ember.Widget.extend
  templateName: 'ember/widgets/navlist/template'

  items: null

  init: ->
    @_super();
    @setIfUnset('navGroups', []);
