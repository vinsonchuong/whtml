get = Ember.get
set = Ember.set

App.Widget.EmailList = Ember.Widget.Accordion.extend
  tagName: 'ul'
  templateName: 'ember/widgets/email_list/template'
  classNames: ['app-widget-emailList']

  toggleItem: (event) ->
    @_super(event)
    set(event.widget.get('bindingContext'), 'read', true)
  deleteEmail: (event) ->
    @remove('items', event.widget.get('bindingContext'))
  reply: (event) ->
    button = event.widget
    get(button.get('bindingContext'), 'replies').pushObject(
      sender: 'You'
      content: button.$().siblings('textarea').val()
      read: true
      starred: false
      replies: []
    )

