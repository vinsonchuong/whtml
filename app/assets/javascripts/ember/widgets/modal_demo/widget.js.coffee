App.Widget.ModalDemo = Ember.Widget.extend
  templateName: 'ember/widgets/modal_demo/template'
  classNames: ['app-widget-modalDemo']

  open: false

  init: ->
    @_super()
    window.modalDemo = this

  openModal: -> @set('open', true)
  closeModal: -> @set('open', false)
