get = Ember.get

App.Widget.EmailClient = Ember.Widget.extend
  templateName: 'ember/widgets/email_client/template'
  classNames: ['app-widget-emailClient']

  selectedTab: 0
  inbox: [
    {sender: 'Vinson Chuong', subject: 'Email 1', content: 'This is an email.', read: false, starred: false, replies: [
      {sender: 'David Thorman', subject: 'Reply 1.1', content: 'This is a reply.', read: false, starred: false, replies: []},
      {sender: 'David Thorman', subject: 'Reply 1.2', content: 'This is a reply.', read: false, starred: false, replies: []},
    ]},
    {sender: 'Vinson Chuong', subject: 'Email 2', content: 'This is an email.', read: false, starred: false, replies: []},
    {sender: 'Vinson Chuong', subject: 'Email 3', content: 'This is an email.', read: true, starred: true, replies: []}
  ]
  unreadCount: Ember.computed(-> @filter('inbox', 'read', false).length).property('inbox.@each.read')
  starred: Ember.computed(-> @filter('inbox', 'starred', true)).property('inbox.@each.starred')
  query: ''
  searchResults: Ember.computed(->
    query = @get('query')
    return [] if query is ''
    @filter('inbox', (email) ->
      (get(email, 'subject') +
        ' ' + get(email, 'sender') +
        ' ' + get(email, 'content') +
        ' ' + get(email, 'replies').reduce((result, reply) ->
          result + ' ' + get(reply, 'sender') + ' ' + get(reply, 'content')
        , '')).indexOf(query) isnt -1
    )
  ).property('query', 'inbox.@each')

  search: -> @set('selectedTab', 2)
