App.Widget.NavListItem = Ember.Widget.extend
  tagName: 'li'
  templateName: 'ember/widgets/navlist_item/template'
  classNameBindings: ['itemClass']

  active: false
  type: null

  itemClass: Ember.computed(->
    type = @get('type')
    if type?
      if type is 'header'
        return 'nav-header'
      else if type is 'divider'
        return 'divider'
    else if @get('active')
      return 'active'
    else
      return '')
