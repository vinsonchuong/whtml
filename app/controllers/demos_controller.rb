class DemosController < ApplicationController
  # GET /demos
  def index
  end

  # GET /show/:name
  def show
    render params[:name]
  end
end