WHTML::Application.routes.draw do
  match 'demos' => 'demos#index', :via => :get
  match 'demos/:name' => 'demos#show', :via => :get

  root :to => 'demos#index'
end
